import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { FooterComponent } from './components/footer/footer.component';
import { SingleModule } from './single/single.module';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    FooterComponent,
    // HeroComponent,
    // HistoriaComponent,
    // ProjetoComponent,
    // EquipeComponent,
    // ContatoComponent,
  ],
  imports: [BrowserModule, AppRoutingModule, SingleModule],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
