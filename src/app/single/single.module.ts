import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SingleComponent } from './single.component';
import { HeroComponent } from '../components/hero/hero.component';
import { HistoriaComponent } from '../components/historia/historia.component';
import { ProjetoComponent } from '../components/projeto/projeto.component';
import { EquipeComponent } from '../components/equipe/equipe.component';
import { ContatoComponent } from '../components/contato/contato.component';
@NgModule({
  declarations: [
    SingleComponent,
    HeroComponent,
    ProjetoComponent,
    HistoriaComponent,
    EquipeComponent,
    ContatoComponent,
  ],
  imports: [CommonModule],
})
export class SingleModule {}
