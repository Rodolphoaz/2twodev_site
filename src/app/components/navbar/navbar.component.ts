import { Component, OnInit } from '@angular/core';
import { objRouteArrayList } from './routeList';
@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss'],
})
export class NavbarComponent implements OnInit {
  routeList;
  constructor() {
    this.routeList = objRouteArrayList;
  }

  ngOnInit(): void {}
}
