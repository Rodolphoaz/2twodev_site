export const objRouteArrayList = [
  {
    title: 'Projetos',
    route: 'projetos',
  },
  {
    title: 'Quem somos',
    route: 'historia',
  },
  {
    title: 'Nossa Equipe',
    route: 'historia',
  },
  {
    title: 'Contatos',
    route: 'contact',
  },
];
